#!/bin/bash -e

# Instalo phpmailer
echo -e "Instalando PHPMailer"
cd /opt/drupal
/usr/local/bin/composer require drupal/smtp

exec "$@"
