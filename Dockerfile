ARG DRUPAL_VERSION=
FROM drupal:${DRUPAL_VERSION}

MAINTAINER Lu Pa <admin@tedic.org>

ENV DEBIAN_FRONTEND noninteractive
ENV APCU_VERSION 5.1.24
ENV UP_VERSION 2.0.2
# Ver aquí las versiones:
# https://pecl.php.net/package/APCu
# http://pecl.php.net/package/uploadprogress

RUN apt update && apt dist-upgrade -y \
  curl \
  wget \
  unzip \
  git \
  vim \
  mariadb-client \
  && apt-get clean

# Instalo cache apcu que recomienda D8
RUN pecl install apcu-$APCU_VERSION uploadprogress-$UP_VERSION\
  && docker-php-ext-enable apcu uploadprogress

# Descargo e instalo php composer
COPY install-composer.sh /
RUN /install-composer.sh 

# Para colocar conf de php especificas
ADD customphp.ini /usr/local/etc/php/conf.d/

COPY docker-entrypoint.sh /

WORKDIR /var/www/html

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["apache2-foreground"]
